<?php //READ//

// Nombre d'enregistrements à afficher sur chaque page
function recordmax()
{
    return 5;
}

function read($page)
{
    require_once 'functions.php';
    // Connectez-vous à la base de données MySQ
    $pdo = pdo_connect_mysql();

    $records_per_page = recordmax();

    // Récupère la page via la requête GET (URL param: page), si elle n'existe pas, la page par défaut est 1
    // Préparez l'instruction SQL et récupérez les enregistrements de notre table de utilisateur, LIMIT déterminera la page
    $stmt = $pdo->prepare('SELECT * FROM utilisateur ORDER BY id_Utilisateur LIMIT :current_page, :record_per_page');
    $stmt->bindValue(':current_page', ($page - 1) * $records_per_page, PDO::PARAM_INT);
    $stmt->bindValue(':record_per_page', $records_per_page, PDO::PARAM_INT);
    $stmt->execute();
    // Récupérez les enregistrements afin que nous puissions les afficher dans notre modèle.
    $utilisateurs = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $utilisateurs;
    // Obtenez le nombre total de utilisateur, cela nous permet de déterminer s'il doit y avoir un bouton suivant et précédent
}

function userread()
{
    $pdo = pdo_connect_mysql();
    $num_utilisateurs = $pdo->query('SELECT COUNT(*) FROM utilisateur')->fetchColumn();
    return $num_utilisateurs;
}
?>

<?php //CREATE//
// Publier les données non vides insérer un nouvel enregistrement

//$MSG//
function recupMSG(){
    $msg = '';
    return $msg;
}

function create($id,$nom,$prenom,$adresse)
{
    require_once 'functions.php';
    $pdo = pdo_connect_mysql();
    recupMSG();
    // Vérifiez si les données POST ne sont pas vides
    if (!empty($_POST)) {
        
        // Insérer un nouvel enregistrement dans la table des contacts
        $stmt = $pdo->prepare('INSERT INTO utilisateur (Nom,Prenom,Adresse) VALUES (?, ?, ?)');
        $stmt->execute([$nom, $prenom, $adresse]);
        // Message de sortie
        $msg = 'Créer avec succé!';
    }
}
?>

<?php
//UPDATE//

function recupuser($id){
    require_once 'functions.php';
    $pdo = pdo_connect_mysql();
    // Récupérez l'utilisateur dans le tableau des utilisateur
    $stmt = $pdo->prepare('SELECT * FROM utilisateur WHERE id_Utilisateur = '.$id.'');
    $stmt->execute();
    $utilisateur = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$utilisateur) {
        exit('User doesn\'t exist with that ID!');
    }
    return $utilisateur;
}

function update($id,$nom,$prenom,$adresse)
{
    require_once 'functions.php';
    $pdo = pdo_connect_mysql();
    $msg = '';
    // Vérifiez si l'ID de utilisateur existe, par exemple update.php? Id = 1 obtiendra l'utilisateur avec l'ID de 1
    if (isset($_GET['id'])) {
        if (!empty($_POST)) {

            // Mettre à jour l'enregistrement
            $stmt = $pdo->prepare('UPDATE utilisateur SET id_Utilisateur = ?, Nom = ?, Prenom = ?, Adresse = ? WHERE id_Utilisateur = ?');
            $stmt->execute([$id, $nom, $prenom, $adresse, $_GET['id']]);
            $msg = 'Updated Successfully!';
        }

    } 
    else {
        exit('No ID specified!');
    }
}
?>

<?php
function delete($id)
{
    require_once 'functions.php';
    $pdo = pdo_connect_mysql();
    $msg = '';
    
        // Assurez-vous que l'utilisateur confirme avant la suppression
        
            
                // L'utilisateur a cliqué sur le bouton "Oui", supprimer l'enregistrement
                $stmt = $pdo->prepare('DELETE FROM utilisateur WHERE id_Utilisateur = '.$id.'');
                $stmt->execute();
                $msg = 'You have deleted the user!';
            
}
?>