<?php
define('ROOTSERVER', '/newbookmark');
require_once('controller.php');
if(isset($_GET['page'])){	
	switch ($_GET['page']){
		case 'home':
			home();
			break;	
		
		case 'read':
			pageread($_GET['id']);
			break;

		case 'create':
		
			// Configurez les variables qui vont être insérées, nous devons vérifier si les variables POST existent sinon nous pouvons les vider par défaut
			$id = isset($_POST['id_Utilisateur']) && !empty($_POST['id_Utilisateur']) && $_POST['id_Utilisateur'] != 'auto' ? $_POST['id_Utilisateur'] : NULL;
			// Vérifiez si la variable POST "nom" existe, sinon la valeur par défaut est vide, fondamentalement la même pour toutes les variables
			$nom = isset($_POST['nom']) ? $_POST['nom'] : '';
			$prenom = isset($_POST['prenom']) ? $_POST['prenom'] : '';
			$adresse = isset($_POST['adresse']) ? $_POST['adresse'] : '';
			createcontrol($id,$nom,$prenom,$adresse);
			break;
		
		case 'Creer':
			require_once('views/create.php');
			break;

		case 'update':
			 // Cette partie est similaire à create.php, mais à la place, nous mettons à jour un enregistrement et n'insérons pas
			 $id = isset($_POST['id_Utilisateur']) ? $_POST['id_Utilisateur'] : NULL;
			 $nom = isset($_POST['Nom']) ? $_POST['Nom'] : '';
			 $prenom = isset($_POST['Prenom']) ? $_POST['Prenom'] : '';
			 $adresse = isset($_POST['Adresse']) ? $_POST['Adresse'] : '';
			 modifiercontrol($id,$nom,$prenom,$adresse);
			 break;

		case 'Modifier':
			$id = $_GET['id'];
			updatecontrol($id);
			break;

		case 'delete':
			$id = $_GET['id'];
			deletecontrol($id);
			break;

		case 'confirmDelete':
			$id = $_GET['id'];
			confirmdelete($id);

	}
	
}
?>