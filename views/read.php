<?php
ob_start();
?>

<div class="content read">
	<h2>Liste Utilisateur</h2>
	<a href="<?php echo ROOTSERVER ?>/Creer/" class="create-utilisateur">Créer Utilisateur</a>
	<table>
        <thead>
            <tr>
                <td>#</td>
                <td>Nom</td>
                <td>Prenom</td>
                <td>Adresse</td>
                <td></td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($utilisateurs as $utilisateur): ?>
            <tr>
                <td><?=$utilisateur['id_Utilisateur']?></td>
                <td><?=$utilisateur['Nom']?></td>
                <td><?=$utilisateur['Prenom']?></td>
                <td><?=$utilisateur['Adresse']?></td>
                <td class="actions">
                    <a href="<?php echo ROOTSERVER ?>/Modifier/<?=$utilisateur['id_Utilisateur']?>" class="edit"><i class="fas fa-pen fa-xs"></i></a>
                    <a href="<?php echo ROOTSERVER ?>/delete/<?=$utilisateur['id_Utilisateur']?>" class="trash"><i class="fas fa-trash fa-xs"></i></a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
	<div class="pagination">
		<?php if ($page > 1): ?>
		<a href="<?php echo ROOTSERVER ?>/read/<?=$page-1?>"><i class="fas fa-angle-double-left fa-sm"></i></a>
		<?php  endif; ?>
		<?php  if ($page*$records_per_page < $num_utilisateurs): ?>
		<a href="<?php echo ROOTSERVER ?>/read/<?=$page+1?>"><i class="fas fa-angle-double-right fa-sm"></i></a>
		<?php  endif; ?>
	</div>
</div>


<?php
$Mycontent = ob_get_clean();
require 'views/template.php'
?>
