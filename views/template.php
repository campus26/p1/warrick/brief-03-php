<!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8">
            <title>Base de Données</title>
            <link href="<?php echo ROOTSERVER ?>/style.css" rel="stylesheet" type="text/css">
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
        </head>
        <body>
        <nav class="navtop">
            <div>
                <h1>Base de Données</h1>
                <a href="/newbookmark/home"><i class="fas fa-home"></i>Home</a>
                <a href="/newbookmark/read/1"><i class="fas fa-address-book"></i>Utilisateurs</a>
            </div>
        </nav>
         
        <?= $Mycontent ?>

        </body>
    </html>