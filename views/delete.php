<?php
ob_start();
?>

<div class="content delete">
	<h2>Supprimer Utilisateur #<?=$utilisateur['id_Utilisateur']?></h2>
    <?php //if ($msg): ?>
    <p><?//=$msg?></p>
    <?php //else: ?>
	<p>Etes vous sur de vouloir supprimer l'utilisateur #<?=$utilisateur['id_Utilisateur']?>?</p>
    <div class="yesno">
        <a href="<?php echo ROOTSERVER ?>/confirmDelete/<?=$utilisateur['id_Utilisateur']?>">Oui</a>
        <a href="<?php echo ROOTSERVER ?>/read/1">Non</a>
    </div>
    <?php //endif; ?>
</div>

<?php
$Mycontent = ob_get_clean();
require 'views/template.php'
?>